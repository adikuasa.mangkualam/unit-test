package com.infosys.learning.servicetest;

import com.infosys.learning.model.User;
import com.infosys.learning.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class UserServiceTest {

    @Autowired
    private UserRepository userRepository;

    public User getUser(int id) {
        return userRepository.getOne(id);
    }

}
