package com.infosys.learning.dto;

public class PersonRequest extends Person{
    private int yearOfBirth;

    public PersonRequest() {
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

}
