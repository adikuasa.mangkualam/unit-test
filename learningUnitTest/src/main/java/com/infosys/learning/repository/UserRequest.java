package com.infosys.learning.repository;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.util.Objects;


public class UserRequest {


    private  String username;
    private  String password;

    public UserRequest() {
    }



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o){
        if(this == o) return true;
        if (!(o instanceof UserRequest)) return false;
        UserRequest user = (UserRequest) o;
        return Objects.equals(username,user.username)
                &&Objects.equals(password, user.password);
    }
}
