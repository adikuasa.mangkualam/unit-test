package com.infosys.learning.Controller;

import com.infosys.learning.dto.Person;
import com.infosys.learning.model.User;
import com.infosys.learning.repository.UserRepository;
import com.infosys.learning.service.LearningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import com.infosys.learning.repository.UserRequest;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class LearningController {

    @Autowired
    LearningService learningService;

    @Autowired
    UserRepository userRepository;

    @GetMapping(value = "/getpersonnametype2")
    public String getPersonNameType2(@RequestParam(value = "gender",defaultValue = "gender") String genders){
        return learningService.getName(genders);
    }

    @GetMapping(value = "/getpersonname")
    public String getPersonName(@RequestParam(value = "gender", defaultValue = "gender") String gender){
        return learningService.getName(gender);
    }

    @GetMapping(value = "/getpersonnamev2")
    public Person getPersonNameV2(@RequestParam(value = "gender",defaultValue = "gender")String gender){
        return learningService.getNameV2(gender);
    }

    @PostMapping(value ="/getpersonnamev3", consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public String getPersonNamev3(@RequestBody(required = true) Person person){
        return learningService.getNameV3(person.getName());
    }
    
    @PostMapping(value ="/getpersonnamev4", consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)

    public Integer getPersonNamev4(@RequestBody(required = true) Person person) {
        return learningService.tahunLahir(person.getAngka());
    }

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String register(@RequestBody(required = true)UserRequest userRequest){
        return learningService.register(userRequest);
    }

    @GetMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String login(@RequestBody(required = true) UserRequest userRequest){

        return learningService.login(userRequest);

    }


}
